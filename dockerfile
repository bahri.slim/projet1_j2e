FROM tomcat:9.0-jdk11-corretto-al2
WORKDIR /usr/local/tomcat/webapps
COPY webapp.war .
EXPOSE 8080
